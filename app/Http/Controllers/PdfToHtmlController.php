<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use PDF;

include '../vendor/autoload.php';
use Gufy\PdfToHtml\Config;
use Gufy\PdfToHtml\Pdf;

class PdfToHtmlController extends Controller
{
    public function export(){

        $data = [];
        $pdf = PDF::loadView('export', $data);
        return $pdf->download('sample.pdf');

    }

    public function pdf(){

        // // change pdftohtml bin location
        // Config::set('pdftohtml.bin', '/poppler/bin/pdftohtml.exe');

        // // change pdfinfo bin location
        // Config::set('pdfinfo.bin', '/poppler/bin/pdfinfo.exe');

        // // initiate
        // $pdf = new Pdf('/storage/pdf/sample.pdf'); 

        // // convert to html and return it as [Dom Object](https://github.com/paquettg/php-html-parser)
        // $html = $pdf->html(); 

        // // // check if your pdf has more than one pages
        // // $total_pages = $pdf->getPages();

        // // // Your pdf happen to have more than one pages and you want to go another page? Got it. use this command to change the current page to page 3
        // // $html->goToPage(3);

        // // and then you can do as you please with that dom, you can find any element you want
        // $paragraphs = $html->find('body > p');

        // initiate
        $pdf = new \TonchikTm\PdfToHtml\Pdf('/storage/pdf/sample.pdf', [
            'pdftohtml_path' => '/poppler/bin/pdftohtml.exe',
            'pdfinfo_path' => '/poppler/bin/pdfinfo.exe'
        ]);
// dd( $pdf );

        // get content from all pages and loop for they
        foreach ($pdf->getHtml()->getAllPages() as $page) {
            echo $page . '<br/>';
        }

    }
}
